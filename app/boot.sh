#!/bin/sh 
echo "======== start clean docker containers logs ========"  

logs=$(find /var/lib/docker/containers/ -name *-json.log)  

for log in $logs  
        do  
                echo "clean logs : $log"  
                cat /dev/null > $log  
        done  

echo "======== end clean docker containers logs ========"

echo "======== start docker containers ========"
docker-compose up -d
echo "======== docker containers started ========"

echo "======== docker remove none images started ========"
docker ps -a | grep "Exited" | awk '{print $1 }'|xargs docker stop
docker ps -a | grep "Exited" | awk '{print $1 }'|xargs docker rm
docker images|grep none|awk '{print $3 }'|xargs docker rmi
echo "======== docker remove none images end ========"

docker restart eureka-server